# us26_week10

## Description
The `week10` Docker container hosts a Python application that provides sentiment analysis functionality. It listens on port 8080 within the container and can be accessed externally through port 3030.

## Usage
To run the `week10` container, execute the following command:

```bash
sudo docker run --rm -p 3030:8080 week10
```

This command starts the container, exposes port 8080 of the container to port 3030 on the host system, and automatically removes the container after it stops.

## Accessing the Service
Once the container is running, you can access the sentiment analysis service using HTTP requests. The service expects POST requests with JSON payloads containing text to analyze. The response will include sentiment analysis results.

Example POST request:
```bash
curl -X POST \
  http://localhost:3030/sentiment \
  -H 'Content-Type: application/json' \
  -d '{
    "text": "I love this product! It's amazing."
}'
```

Example response:
```json
{
  "sentiment": "positive",
  "confidence": 0.85
}
```

## Integration with AWS Lambda and API Gateway
You can also integrate this sentiment analysis service with AWS Lambda and API Gateway for scalable and serverless deployment.

1. **Deploy the Docker Container**: Deploy the `week10` Docker container to a cloud provider or any server that supports Docker. Ensure it's accessible over the internet.

2. **Create an AWS Lambda Function**: Create a new Lambda function using the AWS Management Console or AWS CLI. Use a runtime compatible with your sentiment analysis code (e.g., Python). Inside the Lambda function, invoke the sentiment analysis service hosted in the Docker container using HTTP requests.

3. **Create an API Gateway Endpoint**: Set up an API Gateway endpoint to trigger the Lambda function. Define the necessary routes and methods to expose the sentiment analysis service as a RESTful API.

4. **Test the Integration**: Test the integration by sending HTTP requests to the API Gateway endpoint. Verify that the sentiment analysis service works as expected.

![alt text](Images/Screenshot_2024-04-14_at_3.17.44_PM.png)

![alt text](Images/Screenshot_2024-04-14_at_3.12.49_PM.png)

![alt text](Images/Screenshot_2024-04-14_at_3.11.01_PM.png)



