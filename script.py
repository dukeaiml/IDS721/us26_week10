import json
import tempfile
from transformers import pipeline
import os

# Load the sentiment analysis model
model = pipeline(
    "sentiment-analysis", model="distilbert-base-uncased-finetuned-sst-2-english"
)

# Set the temporary directory
tempfile.tempdir = "/tmp"
os.environ["TRANSFORMERS_CACHE"] = "/tmp"


def lambda_handler(event, context):
    # Get input text from the event
    input = event.get("input", "")

    # Perform sentiment analysis
    sentiment_result = model(input)

    # Return the result
    return {
        "statusCode": 200,
        "body": json.dumps({"sentiment_result": sentiment_result}),
    }


if __name__ == "__main__":
    import sys

    # Check if any command line argument is provided
    if len(sys.argv) > 1:
        input_text = sys.argv[1]
    else:
        input_text = "Input the text"

    # Setting a dummy context
    context = {}
    # Create an event dictionary
    event = {"input_text": input_text}
    # Call the lambda handler
    result = lambda_handler(event, context)
    print(result)
